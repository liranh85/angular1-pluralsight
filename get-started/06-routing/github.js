(function() {
    var github = function($http) {
        var auth = {
            Authorization: "Basic 160be7b6af540f1d455befb6e9c0b6a754c1abe4"
        };

        var getUser = function(username) {
            return $http.get("https://api.github.com/users/" + username, { headers: auth })
                .then(function(response) {
                    return response.data;
                });
        };

        var getRepos = function(user) {
            return $http.get(user.repos_url)
                .then(function(response) {
                    return response.data;
                });
        };

        getRepoDetails = function(username, reponame) {
            var repo;
            var repoUrl = "https://api.github.com/repos/" + username + "/" + reponame;

            return $http.get(repoUrl)
                .then(function(response) {
                    repo = response.data;

                    return $http.get(repoUrl + "/contributors");
                })
                .then(function(response) {
                    repo.contributors = response.data;
                    return repo;
                });
        };

        return {
            getUser: getUser,
            getRepos: getRepos,
            getRepoDetails: getRepoDetails
        };
    }

    var module = angular.module("githubViewer");
    // Register service with Angular
    module.factory("github", github);
})();